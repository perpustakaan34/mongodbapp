Aplikasi Microservices Sederhana Perpustakaan
-> Aplikasi pinjam buku sederhana dengan mencatat secara personal buku yang dipinjam dan dikembalikan.

Pada services ini akan mengelola buku-buku yang tersedia dimana pada services ini terdapat juga beberapa fitur yang disediakan

Fungsi
- tabel books : melihat semua buku, melihat buku berdasarkan bookid, melihat buku dengan nama, menambahkan, mengubah, dan menghapus data buku

Persiapan dua services
- Untuk mongodbapp menggunakan tugas yang sudah pernah dikerjakan

Cara Penggunaan
Untuk menggunakannya, tinggal mengakses localhost:8000 dan sesuai fungsi apa yang ingin digunakan

